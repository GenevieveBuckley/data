# data

Data files used by the scikit-image project

`Normal_Epidermis_and_Dermis_with_Intradermal_Nevus_10x.JPG`

- origin: https://en.wikipedia.org/wiki/File:Normal_Epidermis_and_Dermis_with_Intradermal_Nevus_10x.JPG
- license: public domain
- description: hematoxylin and eosin stained slide at 10x of normal epidermis and dermis with a benign intradermal nevus

`AS_09125_050116030001_D03f00d0.tif`

- origin: https://github.com/CellProfiler/examples/blob/master/ExampleHuman/images/AS_09125_050116030001_D03f00d0.tif
- license: CC0
- description: microscopy image of human cells provided by Jason Moffat
through [CellProfiler](https://cellprofiler.org/examples/#human-cells):

Moffat J, Grueneberg DA, Yang X, Kim SY, Kloepfer AM, Hinkle G, Piqani
B, Eisenhaure TM, Luo B, Grenier JK, Carpenter AE, Foo SY, Stewart SA,
Stockwell BR, Hacohen N, Hahn WC, Lander ES, Sabatini DM, Root DE
(2006) "A lentiviral RNAi library for human and mouse genes applied to
an arrayed viral high-content screen" Cell, 124(6):1283-98.
PMID: 16564017
:DOI:`10.1016/j.cell.2006.01.040`

`astronaut_rl.npy`

- description: testdata for [skimage/restoration/tests/test_restoration](https://github.com/scikit-image/scikit-image/blob/master/skimage/restoration/tests/test_restoration.py)
